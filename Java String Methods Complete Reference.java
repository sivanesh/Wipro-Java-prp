
package checker;

public class Checker {

    public static void main(String[] args) {
        String s = "  Hello  ";
        int i;
        int Character = 3;
        System.out.println(Character);
        
        /*
            In String all methods create a new Object if altered
            For eg for methods like 
                s.toUpperCase();
                s.toLowerCase();
                s.replaceFirst();
                which alters the string obj and creates a new object hence inorder to proceed through those stuffs it should be referenced as
            s = s.toLowerCase();
        
        */
        
        // Trim
        System.out.println("Trim");
        System.out.println(s);
        System.out.println(s.trim());
        s = s.trim();
        
        // To UpperCase nd LowerCase
        System.out.println("\nUppercase and Lower case");
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
        
        // Substring
        System.out.println("\nSubstring");
        System.out.println(s.substring(2));
        System.out.println(s.substring(2, 4));
        
        // Starts with ends with
        System.out.println("\nStarts with and ends with");
        System.out.println("Starts with Hel " + s.startsWith("Hel"));
        System.out.println("Ends with lo " + s.endsWith("lo"));
        // Starts with and ends with with an index
        System.out.println("Starts with lo " + s.startsWith("lo", 3));
        /* Ends with doesnt have a index second parameter"); */
        
        /* Split() 's arguments support regex!!! */
        
        // Split with index
        String[] sArray = s.split("");
        System.out.println("\nSplitting each character");
        printArray(sArray);
        
        sArray = s.split("", 3);
        /* .split(regex, n) => splits this upto n - 1 instance */
        System.out.println("\nSplitting each character with index 3");
        printArray(sArray);
        System.out.println("");
        
        
        // Replace first 
        /* .replaceFirst(regex, replaceString) Replaces the first occurenced of the regex alone and change them */
        System.out.println("\nReplace first");
        System.out.println(s.replaceFirst("Hel", "000"));
        
        // Replace All
        /* Replace all replaces all occurences to this string */
        String ss = "Hello hello Hello Hello";
        System.out.println("\nReplaceAll");
        System.out.println(ss.replaceAll("Hel", "000"));
        
        
        // Replace(char old, char new)
        /* Difference between replace("ss", "00") and replaceAll("ss" ,"oo") is
            replaceMethod (1) takes charSequence as two inputs hence String is implicitely converted to CharSequence
        */
        System.out.println("\nReplace ");
        System.out.println(ss.replace('l', 'z'));
        
        // Region matches
        System.out.println("\nREGION MATCHES");
        
        /* str.regionMatches(strOffset, otherStr, otherStrOffset, length to be verified) */
        System.out.println(ss.regionMatches(0, s, 0, 3));
        System.out.println(ss.regionMatches(0, s, 1, 3));
        
        System.out.println("\tIGNORE CASE");
        System.out.println(ss.regionMatches(true, 6, ss, 0, 3));
        
        
        // str.matches(regex)
        System.out.println("\nMATCHES");
        System.out.println(ss.matches("Hell"));
        
        // Last index
        System.out.println("\nLAST INDEX");
        System.out.println(ss.lastIndexOf("el"));
        
    }
    
    static void printArray(String[] s) {
        for(int i = 0; i < s.length; i++) {
            System.out.print(s[i] + "  ");
        }
    }
    
}
