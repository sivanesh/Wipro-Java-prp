package module;

public class WiproStrings9 {
    
    public static void main(String[] args) {
        StringBuilder s =  new StringBuilder("ab**e*de");
        int i;
        
        for(i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '*') {
                if(i - 1 >= 0 && (s.charAt(i - 1) != '*')) s.replace(i - 1, i, "?");
                if(i + 1 < s.length() && (s.charAt(i + 1) != '*')) s.replace(i + 1, i + 2, "?"); 
            }
        }
        
        for(i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '?' || s.charAt(i) == '*') {
                s.replace(i, i + 1, "");
                i--;
            }
        }
        
        System.out.println(s);
    }
}