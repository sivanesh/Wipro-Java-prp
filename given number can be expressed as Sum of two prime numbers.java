package Home;

import java.util.Arrays;

public class Base {
	public static void main(String args[]) {
		// Check whether the given number can be expressed as Sum of two prime numbers
		
		int a = 23;
		
		System.out.println(isRepresented(a));
		
	} 
	
	private static boolean isRepresented(int a) {
		int i, j;
		
		int ar[] = primeNumbers(a);
		for(i = 0; i < ar.length; i++) {
			for(j = 0; j < ar.length; j++) {
				if(ar[i] + ar[j] == a) return true;
			}
		}
		return false;
	}
	
	private static int[] primeNumbers(int a) {
		int i, j, pointer = 0;
		int primes[] = new int[a];
		boolean isPrime;
		for(i = 1; i < a; i++) {
			isPrime = true;
			if(i != 2 && i % 2 == 0) continue;
			for(j = 2; j < i / 2; j++) {
				if(i % j == 0) {
					isPrime = false;
					break;
				}
			}
			if(isPrime) primes[pointer++] = i;
		}
		return Arrays.copyOf(primes, pointer);
	}
}