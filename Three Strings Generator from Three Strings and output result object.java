import java.io.*;
import  java.util.*;

// Read only region start
class UserMainCode
{

	public class Result{
		public final String output1;
		public final String output2;
		public final String output3;

		public Result(String out1, String out2, String out3){
			output1 = out1;
			output2 = out2;
			output3 = out3;
		}
	}
	
    public Result encodeThreeStrings(String input1,String input2,String input3){
    	// Read only region end
        StringBuilder output1 = new StringBuilder();
		StringBuilder output2 = new StringBuilder();
		StringBuilder output3 = new StringBuilder();
		String[] arr = new String[] {input1, input2, input3};
		
		int i, seperator = 1;
		
		for(i = 0; i < arr.length; i++) {
			if(arr[i].length()  % 3 == 0) {
				// divide exactly three parts
				output1.append(arr[i].substring(0, arr[i].length() / 3));
				output2.append(arr[i].substring(arr[i].length() / 3, arr[i].length() * 2 / 3));
				output3.append(arr[i].substring(arr[i].length() * 2 / 3));
			} else if(arr[i].length() % 3 == 1) {
				// one character extra middle one has
				output1.append(arr[i].substring(0, arr[i].length() / 3));
				output2.append(arr[i].substring(arr[i].length() / 3, arr[i].length() * 2 / 3 + 1));
				output3.append(arr[i].substring(arr[i].length() * 2 / 3 + 1));
			} else {
				// two characters extra
				output1.append(arr[i].substring(0, arr[i].length() / 3 + 1));
				output2.append(arr[i].substring(arr[i].length() / 3 + 1, arr[i].length() * 2 / 3));
				output3.append(arr[i].substring(arr[i].length() * 2 / 3));
			}
		}
		
		// case changer for output3
		char c;
		for(i = 0; i < output3.length(); i++) {
			c = output3.charAt(i);
			if(Character.isUpperCase(c)) output3.setCharAt(i, Character.toLowerCase(c));
			else output3.setCharAt(i, Character.toUpperCase(c));
		}
		
        return new Result(output1.toString(), output2.toString(), output3.toString());
    }
}