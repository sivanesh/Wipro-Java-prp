
package base;

public class Base {

    public static void main(String[] args) {
        
        int ar[] = {12, 1313, 122, 678, 898};
        int i, stableSum = 0, nonStableSum = 0, min = 100000, max = -1;
        
        for(i = 0; i < ar.length; i++) {
            if(isStable(ar[i])) {
                if(ar[i] > max) max = ar[i];
                if(ar[i] < min) min = ar[i];
            } 
        }
        
        System.out.println(max + min);
        
    }
    
    static boolean isStable(int a) {
        int nums[] = new int[10];
        
        int rem, max = -1, i;
        
        while(a > 0) {
            rem = a % 10;
            nums[rem]++;
            a /= 10;
        }
        
        for(i = 0; i < nums.length; i++) {
            if(nums[i] > max) max = nums[i];
        }
       
        
        for(i = 0; i < nums.length; i++) {
            if(nums[i] > 0 && nums[i] != max) return false;
        }
        return true;
    }
    
}
