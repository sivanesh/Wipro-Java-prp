import java.util.Scanner;

public class Hello {
    public static void main(String[] args) {
        int n = 5;
        int a[][] = new int[n][n];
        int b[][] = new int[n][n];
        
        int i, j, k;
        
        Scanner d = new Scanner(System.in);
        
        // changing b values to -1
        for(i = 0; i < n; i++) {
            for(j = 0; j < n; j++) {
                b[i][j] = -1;
            }
        }
        
        // Input and zeroes assignment
        for(i = 0; i < n; i++) {
            for(j = 0; j < n; j++) {
                a[i][j] = d.nextInt();
                if(a[i][j] == 0) {
                    // X Axis to 0
                    for(k = 0; k < n; k++) {
                        b[i][k] = 0;
                    }
                    
                    // Y Axis to 0
                    for(k = 0; k < n; k++) {
                        b[k][j] = 0;
                    }
                    
                    
                }
            }
        }
        
        // print current matrix (no needed just for viewing)
        System.out.println("Current matrix is");
        for(i = 0; i < n; i++) {
            System.out.println("");
            for(j = 0; j < n; j++) {
                System.out.print(a[i][j] + "\t");
            }
        }
        
        
        System.out.println("\nOutput is");
        for(i = 0; i < n; i++) {
            System.out.println("");
            for(j = 0; j < n; j++) {
                if(b[i][j] != 0) {
                    b[i][j] = a[i][j];
                }
                System.out.print(b[i][j] + "\t");
            }
        }

    }
}
