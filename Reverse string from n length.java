import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        String s = d.nextLine();
        int n = d.nextInt();
        
        // implementations
        int i, j;
        for(i = 0; i < s.length(); i+= n)
            for(j = i + n - 1; j >= i; j--) 
                if(j < s.length()) System.out.print(s.charAt(j));
    }
    
}