
package javaapplication2;

import java.util.*;


public class JavaApplication2 extends Thread {

  
    public static void main(String[] args) {
        int a[] = new int[] {4, 2, 1, 5, 9, 3, 7, 6};
        
        int i, j, temp, count = 0;
        int ans[] = new int[a.length];
        for(i = 0; i < a.length; i++) {
            for(j = 0; j < a.length; j++) {
                if(a[i] > a[j]) {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }   
            }
            
        }
        
        for(i = 0; i < a.length / 2 + 1; i++) {
            if(count < ans.length)
            ans[i] = a[count++];
            if(count < ans.length)
            ans[a.length - i - 1] = a[count++];
        }
        
        for(i = 0; i < a.length; i++) {
            System.out.print(ans[i] + "\t");
        }
        
        
        
    }
    
    
}