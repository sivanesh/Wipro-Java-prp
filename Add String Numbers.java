import java.io.*;
import  java.util.*;

// Read only region start
class UserMainCode
{

	public String addNumberStrings(String input1,String input2){
		// Read only region end
		int n = input1.length();
		int m = input2.length();
		
		StringBuilder ans = new StringBuilder();
		int a, b, add, carry = 0;
		n--;
		m--;
		while(n >= 0 || m >= 0) {
			if(n >= 0) {
				a = Integer.parseInt(input1.charAt(n) + "");
			} else a = 0;
			
			if(m >= 0) {
				b = Integer.parseInt(input2.charAt(m) + "");
			} else b = 0;
			System.out.println("a = " + a + " b = " + b + " carry " + carry);
			add = a + b + carry;
			if(add < 10) {
				ans.append(add);
				carry = 0;
			} else {
				carry = 1;
				ans.append(add % 10);
			}
		      n--; m--;
		}
		if(carry > 0) {
			ans.append(carry);
		}
		return ans.reverse().toString();
	}
}