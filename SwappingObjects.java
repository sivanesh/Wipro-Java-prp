public class SwapObjects {
    public static void main(String[] args) {
        Details obj1 = new Details("Eleven", "physiatrist", 11);
        Details obj2 = new Details("Maddy", "Playboy", 11);

        System.out.println("Details before swapping");
        prinObjects(obj1, obj2);

        // Swapping
        Details temp = obj1;
        obj1 = obj2; 
        obj2 = temp;

        System.out.println("After swapping");
        
        prinObjects(obj1, obj2);
    }

    static void prinObjects(Details obj1, Details obj2) {
        System.out.println("Detail 1:");
        obj1.printDetails();

        System.out.println("Detail 2:");
        obj2.printDetails();
    }
}

class Details {
    String name;
    String job;
    int id;

    Details(String name, String job, int id) {
        this.name = name;
        this.job = job;
        this.id = id;
    }

    void printDetails() {
        System.out.println("Name = " + this.name + "\tJob = " + this.job + "\tID = " + this.id);
    }

}