package base;

import java.util.Date;

public class Base {

    public static void main(String[] args) {
        
        // First table inputs
        int[] empNo = {1001, 1002, 1003, 1004, 1005, 1006, 1007};
        String[] empName = {"Ashish", "Sushma", "Rahul", "Chahat", "Ranjan", "Suman", "Tanmay"};
        char[] designationCode = {'e', 'c', 'k', 'r', 'm', 'e', 'c'};
        String[] department = {"R&D", "PM", "Acct", "Front Desk", "Engg", "Manufacturing", "PM"};
        int[] basic = {20000, 30000, 10000, 12000, 50000, 23000, 29000};
        int[] hra = {8000, 12000, 8000, 6000, 20000, 9000, 12000};
        int[] it = {3000, 9000, 1000, 2000, 20000, 4400, 10000};
        
//        Designation table
        char[] designationCodeChecker = {'e', 'c', 'k', 'r', 'm'};
        String[] designation = {"Engineer", "Consultant", "Clerk", "Receptionist", "Manager"};
        int[] da = {20000, 32000, 12000, 15000, 40000};
        
        
//        Implementations
        int input, firstIndex, secondIndex, salary;
        char dCode = '?';
        int i;
        
//        input = Integer.parseInt(args[0]);
        input = 1003;
        firstIndex = -1;
        secondIndex = -1;
        
        for(i = 0; i < empNo.length; i++) {
            if(input == empNo[i]) {
                firstIndex = i;
                dCode = designationCode[i];
                break;
            }
        }
        
        for(i = 0; i < designationCodeChecker.length; i++) {
            if(designationCodeChecker[i] == dCode) {
                secondIndex = i;
                break;
            }
        }
        
        salary = basic[firstIndex] + hra[firstIndex] - it[firstIndex] + da[secondIndex];
        
        System.out.println(empNo[firstIndex] + "\t" + empName[firstIndex] + "\t" + department[firstIndex] + "\t" + designation[secondIndex] + "\t" + salary);
        
        
    }
    
}
