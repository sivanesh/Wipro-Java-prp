/*
Tasks

INSERTION
    1. Create character matrix of 25 elements
    2. Add key's unique characters (from left to right) to matrix.
    3. Add remaining characters in the matrix

ENCRYPTION
    1. Split into pairs
    2. Check for duplicates and add x and add x for extra character also.
    3. Follow those three conditions and append them to StringBuilder
        I   If two characters are in same row choose the next character (If in end then take wrap and take the other side element)
        II  If two characters in column then it will be as same as I column wise
        III If it is not in above two cases then A, B elements gets the values of the position of other elements
            As Bth position element ot A and Ath position element to B.

DECRYPTION
    1. Reverse Encryption


METHODS
    char[][] fillMatrix(String s) =>  Does INSERTION's 2, 3
    
    

*/


import java.util.Scanner;

public class PlayFairCipher {
    static StringBuilder cipher = new StringBuilder();
    private static char[][] a = new char[5][5];
    
    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        
        // Input
        System.out.println("Enter the plain text and KEY");
        String plain = d.nextLine();
        String key = d.nextLine();
        
        // Implementations
        fillMatrix(key);
       
        encrypt(plain);
        
        System.out.println("Cipher text is " + cipher);
        
        
    }
    
    private static void fillMatrix(String s) {
        int i, j, k;
        i = j = 0;
        // Fill unique elements
        for(k = 0; k < s.length(); k++) {
            if(Character.isAlphabetic(s.charAt(k)) && k == s.indexOf(s.charAt(k))) {
                if(j >= 5) {
                    i++;
                    j = 0;
                }
                a[i][j] = s.charAt(k);
                j++;
                System.out.print(s.charAt(k));
            }
        }
        
        // fill remaining
        char c = 'a';
        for(; i < 5; i++) {
            for(; j < 5; j++) {
                //  j is the eleminated character
                while(s.indexOf(c) != -1 || c == 'j') {
                    c++;
                }
                a[i][j] = c;
                c++;
            }
            j = 0;
        }
        
        // print matrix
        for(i = 0; i < 5; i++) {
            System.out.println("");
            for(j = 0; j < 5; j++) {
                System.out.print(a[i][j] + "\t");
            }
        }
        
        
    } 
    
    private static void encrypt(String ss) {
        int i, j, k;
        int i1, j1, i2, j2;
        int count = 0;
        i1 = i2 = j1 = j2 = 0;
        char first, second;
        
        StringBuilder s = new StringBuilder();
        for(i = 0; i < ss.length(); i++) {
            if(Character.isAlphabetic(ss.charAt(i))) s.append(ss.charAt(i));
        }
        
        // Even number or odd number of characters
        if(s.length() % 2 != 0) s.append('x');
        
        
        for(i = 0; i < s.length() - 1; i++) {
            first = s.charAt(i);
            if(s.charAt(i) == s.charAt(i + 1)) {
                second = 'x';
                count++;
            } else {
                second = s.charAt(i + 1);
                i++;
            }
            
            // j condition
            if(first == 'j') first = 'i';
            if(second == 'j') second = 'i';
            
            System.out.println("first = " + first + "\t second = " + second);
            
            // Indexes of first and second element in matrix
            for(j = 0; j < 5; j++) {
                for(k = 0; k < 5; k++) {
                    if(a[j][k] == first) {
                        i1 = j;
                        j1 = k;
                    }
                    if(a[j][k] == second) {
                        i2 = j;
                        j2 = k;
                    }
                }
                
            }
            
            
            // Encryption
            if(i1 == i2) {
                cipher.append(a[i1][(j1 + 1) % 5]);
                cipher.append(a[i2][(j2 + 1) % 5]);
            } else if(j1 == j2) {
                cipher.append(a[(i1 + 1) % 5][j1]);
                cipher.append(a[(i2 + 1) % 5][j2]);
            } else {
                cipher.append(a[i1][j2]);
                cipher.append(a[i2][j1]);
            }
            
            
            if(i == s.length() - 2) {
                System.out.println("run");
                if(s.length() + count % 2 != 0) s.append('x');
            }
            
        }
    }
}
