import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner d = new Scanner(System.in);
        String s = d.nextLine();
        int n = d.nextInt();
        char arr[] = new char[s.length()];
        int i;

        for(i = 0; i < arr.length; i++) {
                arr[(i + n) % arr.length] = s.charAt(i);
        }

        System.out.println("ans = " + new String(arr));
    }
    
}