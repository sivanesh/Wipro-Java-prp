import java.io.*;
import  java.util.*;

class LongestIncreasingSequence
{

    public static int output1;
	    
				
    public static void longestIncreasingSeq(int input1,int[] input2){
	    int i;
		int length = 0;
		
		for(i = 0; i < input1; i++) {
			length = findLength(input2, i);
			System.out.println(length);
			if(length > output1) output1 = length + 1;
			length = 0;
		}
    }
	
	static int findLength(int[] a, int index) {
		int length = 0, i;
		
		for(i = index; i < a.length - 1; i++) {
			if(a[i + 1] > a[i]) length++;
			else return length;
		}
		return length;
	}
}