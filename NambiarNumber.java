import java.io.*;
import  java.util.*;

// Read only region start
class UserMainCode
{

	public int nnGenerator(String input1){
    		// Read only region end
            int i = 0, j;
            int firstNumber;
        	int sum = 0;
        	StringBuilder ans = new StringBuilder();
        	
        	while(i < input1.length()) {
        		firstNumber = input1.charAt(i);
        		j = i + 1;
        		sum = firstNumber - '0';
        		
        		while(j < input1.length() && firstNumber % 2 == sum % 2) {
        			sum += input1.charAt(j) - '0';
        			j++;
        		} 
        		ans.append(sum);
        		i = j;
        	}
        	return Integer.parseInt(new String(ans));
	}
}