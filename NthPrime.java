import java.io.*;
import  java.util.*;

// Read only region start
class UserMainCode
{

	public int NthPrime(int input1){
		// Read only region end
		// Write code here...
		int counter = 0;
		int i, j;
        for(i = 2; i < Integer.MAX_VALUE; i++) {
			if(isPrime(i)) counter++;
			if(counter == input1) return i;
		}
		return i;
	}
	
	static boolean isPrime(int n) {
		int i;
		if(n != 2 && n % 2 == 0) return false;
		for(i = 3; i < n / 2; i++) {
			if(n % i == 0) return false;
		}
		return true;
	}
}