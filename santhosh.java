package checker;

public class Checker {

    public static void main(String[] args) {
        
        // Array declaration
        int a[][] = new int[5][5];
        a[0] = new int[]{1,2,3,4,5};
        a[1] = new int[]{6,7,8,9,10};
        a[2] = new int[] {11,12,13,14,15};
        a[3] = new int[] {16, 17, 18, 19, 20};
        a[4] = new int[] {21, 22, 23, 24, 25};
        
        // implementation
        int i, j, k, l;
        int n = 5;
        int current, previous;
        
        
        int len = n / 2;
        len = len % 2 == 0 ? len: len + 1;
        
        for(i = 0; i < len; i++) {
            j = i;
            previous = a[i][j];
            
            // top 
            for(k = 1; k < 5; k++) {
                current = a[i][k];
                a[i][k] = previous;
                previous = current;
                
            }
            
            // right 
            for(k = 1; k < 5; k++) {
                current = a[k][n - j - 1];
                a[k][n - j - 1] = previous;
                previous = current;
            }
            
            // bottom
            for(k = n - 1; k >= 0; k--) {
                current = a[n - i - 1][k];
                a[k][n - j - 1] = previous;
                previous = current;
            }
            
            // right
            for(i = n - 1; i >= 0; i--) {
                current = a[k][n - j - 1];
                a[k][n - j - 1] = previous;
                previous = current;
            }
            
            
            
            
        }
            
             // print matrix
        for(i = 0; i < n; i++) {
            System.out.println("");
            for(j = 0; j < n; j++) {
                System.out.print(a[i][j] + "\t");
            }
        }
    }    
}
