import java.io.*;
import  java.util.*;

// Read only region start
class UserMainCode
{

	public class Result{
		public final int output1;
		public final int output2;

		public Result(int out1, int out2){
			output1 = out1;
			output2 = out2;
		}
	}
	
    public Result decreasingSeq(int[] input1,int input2){
    	// Read only region end
        int count = 1;
        int num = 0;
		int max = 0;
		boolean isDecreasing = false;
		for(int i = 1; i < input2; i++) {
			if(input1[i - 1] > input1[i]) {
				count++;
				if(count > max) max = count;
				isDecreasing = true;
			} else {
				if(isDecreasing) num++;
				isDecreasing = false;
				count = 1;
			}
		}
		
		if(isDecreasing) num++;
		return new Result(num, max);
	}
}