/* 
    IFSC Code generator
    
Inputs: 
Bank Name
Location
Account no.

Output format:
First Main letters of BankName
 + 
Zeroes added
Conditions are (Most occured character length) - (Second most occured character)

+ 
Account number


*/

package tester;

import java.util.Scanner;

public class Tester {


    public static void main(String[] args) {
        // sbionline 
        Scanner d = new Scanner(System.in);
        
        // Inputs 
        String bank = d.nextLine();
        String location = d.nextLine();
        long accountNo = d.nextLong();
        
        // Implementations
        StringBuilder answer = new StringBuilder();
        
        String[] bankNames = bank.split(" ");
        int i, indexLength;
        
        for(i = 0; i < bankNames.length; i++) {
            if(Character.isUpperCase(bankNames[i].charAt(0))) {
                answer.append(bankNames[i].charAt(0));
            }
        }
        
            // Number implementations
            int[] chars = new int[26];
            int max, secondMax;
            
            location = location.toLowerCase();
            
            for(i = 0; i < location.length(); i++) {
                indexLength = location.charAt(i) - 'a';
                if(indexLength < 26 && indexLength >= 0) {
                chars[indexLength]++;
                System.out.println(indexLength);
                }
            }
            
            
            
            max = max(chars);
            
            
            for(i = 0; i < chars.length; i++) {
                if(chars[i] == max) chars[i] = 0;
            }
            
            secondMax = max(chars);
    
            for(i = 0; i < (max - secondMax); i++) {
                answer.append("0");
            }
            
            System.out.println("Max = " + max + "\tSecond max = " + secondMax + "\tAnswer = " + (max - secondMax));
    
            answer.append(accountNo);
            
            System.out.println(answer);
    }

    static int max(int chars[]) {
        int i, max = -1;
        for(i = 0; i < chars.length; i++) {
                if(chars[i] > max) {
                    max = chars[i];
                }
            }
        return max;
    }
    
}

