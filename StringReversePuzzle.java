import java.io.*;
import  java.util.*;

class StringReversePuzzle
{

    public static String output1;
	 			
    public static void reverseWords_andCase(String input1,int input2){
        String array[] = input1.split(" ");
		int i, j;
		int caps[] = new int[input1.length()];
		
		StringBuilder s[] = new StringBuilder[array.length];
		StringBuilder answer = new StringBuilder();
		
		for(i = 0; i < s.length; i++) {
			s[i] = new StringBuilder(array[i]);
			s[i] = s[i].reverse();
			
			if(input2 == 1) {
				s[i] = reverseCase1(s[i], array[i]);
			}
			
			if(i != s.length - 1) answer.append(s[i] + " ");
			else answer.append(s[i]);
		}
		
		output1 = new String(answer);
		
		
		
		
    }	
	
	static StringBuilder reverseCase1(StringBuilder s, String array) {
		char c;
		s = new StringBuilder(s.toString().toLowerCase());
		for(int i = 0; i < s.length(); i++) {
			if(Character.isUpperCase(array.charAt(i))) {
				if(Character.isLetter(s.charAt(i))) {
					s.setCharAt(i, Character.toUpperCase(s.charAt(i)));
				} else {
					s.setCharAt(s.length() - 1 - i, Character.toUpperCase(s.charAt(s.length() - 1 - i)));
				}
			}
		}
		return s;
	}
}