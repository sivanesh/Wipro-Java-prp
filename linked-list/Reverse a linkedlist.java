static SinglyLinkedListNode reverse(SinglyLinkedListNode head) {
        SinglyLinkedListNode n = head;
        SinglyLinkedListNode current = null;
        SinglyLinkedListNode nextElement = null;
        while(n != null) {
            nextElement = n.next;
            n.next = current;
            current = n;
            n = nextElement;
        }
        
        return current;

    }